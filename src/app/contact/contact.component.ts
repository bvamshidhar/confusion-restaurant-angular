import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut, expand, visibility } from '../animations/app.animation';
import {FeedbackService} from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  errMess: string;
  isSpinnerVisible = false;
  isFeedbackVisible = false;
  //visibility = 'shown';
  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackcopy: Feedback = null;
  contactType = ContactType;
  @ViewChild('fform') feedbackFormDirective;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':      'Last Name is required.',
      'minlength':     'Last Name must be at least 2 characters long.',
      'maxlength':     'Last Name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':      'Tel. number is required.',
      'pattern':       'Tel. number must contain only numbers.'
    },
    'email': {
      'required':      'Email is required.',
      'email':         'Email not in valid format.'
    },
  };

  constructor(private fb: FormBuilder,
    private feedbackService: FeedbackService) {        //form builders are used to reduce repetitions and clutter 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      telnum: ['', [Validators.required, Validators.pattern] ],
      email: ['', [Validators.required, Validators.email] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onSubmit() {
    this.isSpinnerVisible = true;
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: '',
      email: '',
      agree: false,
      contacttype: 'None',
      message: ''
    });

    
    this.feedbackService.submitFeedback(this.feedback)
      .subscribe(feedback => {
        this.feedback = feedback;
      },
        errmess => { this.feedback = null; this.errMess = <any>errmess; });
      this.feedbackFormDirective.resetForm();
      console.log(this.feedbackFormDirective)
      // setTimeout(() => {} , 5000)
      
      this.isFeedbackVisible = true;
      setTimeout(()=>{
        console.log(this.feedbackFormDirective)
        // console.log(this.isFeedbackVisible,this.isSpinnerVisible)
        this.isFeedbackVisible=true;
        this.isSpinnerVisible=false;
        setTimeout(()=>this.isFeedbackVisible = false,5000);
        },2000)
        
      }
      


  onValueChanged(data?: any) {                     // this will take in a parameter, possibly. That's why I put a data question mark, meaning that the parameter is optional.
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {    //if you call this with no parameter, all the form error fields will be cleared, and then I would do, "control FormGet"
        // clear previous error message (if any)       //hasOwnProperty is when shme value or something is typed in that field
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {         // this control is feedback form.get field and.error
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';    //whatever form error survey or the corresponding message will be taken and attached in, and then so my form errors JavaScript object will now contain all the error messages attached together when this particular on values changed method runs.
            }
          }
        }
      }
    }
  }
}
