import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProcessHTTPMsgService {

  constructor() { }

  public handleError(error: HttpErrorResponse | any) {
    let errMsg: string;

    if (error.error instanceof ErrorEvent) {    //if the error is an instance of ErrorEvent, then it will contain the error msg
      errMsg = error.error.message;
    }
    else {                                      //if not so, it means that it is coming form the server side
      errMsg = `${error.status} - ${error.statusText || ''} ${error.error}`;    //we are extracting the status information from there. And then,- error.statusText, if the statusText exists, or that'll be an empty string there. And then we will also include the error.error itself, the complete error object itself, right there in the string. And then use that as our error message.
    }

    return throwError(errMsg);                   // Now, once we have the error message, then we will be able to use the throwError to return an error observable to our application.
  }

}
